package com.fah.robot;

public class Robot extends Unit {

    public Robot(Map map, int x, int y,char symbol) {
        super(map, x, y, symbol, false);
        
    }
    public void  up(){
        int y = this.getY();
        y--;
        this.setY(y);
    }
    public void  down(){
        int y = this.getY();
        y++;
        this.setY(y);
    }
    public void  left(){
        int x = this.getX();
        x--;
        this.setX(x);
    }
    public void  right(){
        int x = this.getX();
        x++;
        this.setX(x);

    }
}
