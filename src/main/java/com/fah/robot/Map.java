package com.fah.robot;

public class Map {
    private int widgh;
    private int hieght;
    private Unit units[];
    private int unitCount;

    public Map(int widgh, int hieght) {
        this.widgh = widgh;
        this.hieght = hieght;
        this.unitCount = 0;
        this.units = new Unit[widgh * hieght];
    }

    public Map() {
        this(10, 10);
    }

    public void print() {
        for (int row = 0; row < this.hieght; row++) {
            for (int col = 0; col < this.widgh; col++) {
                printBlock(col, row);
            }
            System.out.println();
        }

    }

    private void printBlock(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if (unit.isOn(x, y)) {
                System.out.print(unit.getSymbol());
                return;
            }
        }
        System.out.print("-");
    }

    public String toString() {
        return "Map(" + this.hieght + "," + this.widgh + ")";
    }

    public void add(Unit unit) {
        if (unitCount == (widgh * hieght))
            return;
        this.units[unitCount] = unit;
        unitCount++;
    }

    public void printUnits() {
        for (int i = 0; i < unitCount; i++) {
            System.out.println(this.units[i]);
        }
    }

    public boolean isOn(int x, int y) {
        return isInwidgh(x) && isInHieght(y);
    }

    public boolean isInwidgh(int x) {
        return x >= 0 && x < widgh;
    }

    public boolean isInHieght(int y) {
        return y >= 0 && y < hieght;
    }

    public boolean hasDominate(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if (unit.isOn(x, y) && unit.dominate()) {
                return true ;
            }

        }
        return false ;
    }


}
